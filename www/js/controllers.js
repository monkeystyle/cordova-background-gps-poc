(function() {
    'use strict';

    angular
        .module('geo')
        .controller('gpsController', GpsController);

    GpsController.$inject = ['$scope', '$ionicPlatform', '$http','$filter'];

    function GpsController($scope, $ionicPlatform, $http,$filter) {
        
        $scope.options = {};
        $scope.prefix = {};
        $scope.prefix.text = "1";
        
        $scope.options.debug = true;
        $scope.options.useActivityDetection = true;

        function log(payload) {
              _LTracker.push({
                  'tag' : $scope.prefix.text,
                  'payload' : payload
                  
              });
        }


        $scope.start = function() {
            
            $scope.stop();
            log('start/restart');

            $ionicPlatform.ready(function() {

                // init 
                navigator.geolocation.getCurrentPosition(
                    function(position) {
                        log(position);
                    }, function(f) {
                        log(f);
                    });


                //Get plugin
                var bgLocationServices = window.plugins.backgroundLocationServices;
                
                var options = {
                    //Both
                    desiredAccuracy: $scope.options.desiredAccuracy || 20, // Desired Accuracy of the location updates (lower means more accurate but more battery consumption)
                    distanceFilter: $scope.options.distanceFilter || 5, // (Meters) How far you must move from the last point to trigger a location update
                    debug: $scope.options.debug , // <-- Enable to show visual indications when you receive a background location update
                    interval: $scope.options.interval || 9000, // (Milliseconds) Requested Interval in between location updates.
                    //Android Only
                    notificationTitle: 'Job On The Run', // customize the title of the notification
                    notificationText: 'Finder jobs...', //customize the text of the notification
                    fastestInterval: $scope.options.fastestInterval || 5000, // <-- (Milliseconds) Fastest interval your app / server can handle updates
                    useActivityDetection: $scope.options.useActivityDetection  // Uses Activitiy detection to shut off gps when you are still (Greatly enhances Battery Life)

                }

                //Congfigure Plugin
                bgLocationServices.configure(options);

                //Register a callback for location updates, this is where location objects will be sent in the background
                bgLocationServices.registerForLocationUpdates(function(location) {
                    log(location);
                }, function(err) {
                    log(err);
                });

                //Register for Activity Updates (ANDROID ONLY)
                //Uses the Detected Activies API to send back an array of activities and their confidence levels
                //See here for more information: //https://developers.google.com/android/reference/com/google/android/gms/location/DetectedActivity
                bgLocationServices.registerForActivityUpdates(function(acitivites) {
                    console.log("We got an BG Update" + JSON.stringify(acitivites));
                    log(JSON.stringify(acitivites));

                }, function(err) {
                    log(err);
                });
               
               log(options);
                bgLocationServices.start();

            });
        }

        $scope.stop = function() {
            log('stop');
            window.plugins.backgroundLocationServices.stop();
        }
    }
})();

